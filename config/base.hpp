class base_aac
{
	name = "AAC Airfield";
	isOwned = 0;
	groups = 4;
	respawnGroups = 1;
	respawnTime = 300;
	vehicles[] = {};
};

class base_abdera
{
	name = "Abdera Airfield";
	isOwned = 0;
	groups = 4;
	respawnGroups = 1;
	respawnTime = 300;
	vehicles[] = {};
};

class base_airport
{
	name = "Airport";
	isOwned = 0;
	groups = 4;
	respawnGroups = 1;
	respawnTime = 300;
	vehicles[] = {};
};

class base_feres
{
	name = "Feres Airfield";
	isOwned = 0;
	groups = 4;
	respawnGroups = 1;
	respawnTime = 300;
	vehicles[] = {};
}

class base_molos
{
	name = "Molos Airfield";
	isOwned = 0;
	groups = 4;
	respawnGroups = 1;
	respawnTime = 300;
	vehicles[] = {};
}